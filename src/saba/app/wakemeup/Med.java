package saba.app.wakemeup;

import java.util.Random;

import saba.app.wakemeup.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Med extends Activity {
	public char op1, op2;

    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.med);
        TextView eq = (TextView) findViewById(R.id.eq);
        
        Button submit = (Button) findViewById(R.id.submit);
        EditText et=(EditText) findViewById(R.id.answer);
        Typeface tf =   Typeface.createFromAsset(getAssets(),"ALGER.TTF");
        
        et.setTypeface(tf);
        eq.setTypeface(tf);
      
        final int n1 = new Random().nextInt(5);
        final int n2 = new Random().nextInt(10);
        final int n3 = new Random().nextInt(15);
        final int OP1 = new Random().nextInt(3);
        final int OP2 = new Random().nextInt(3);
        final Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vib.vibrate(new long[] { 0, 200, 0 }, 0);
        
        switch(OP1)
        {
        case 0: op1 = '+'; break;
        case 1: op1 = '-'; break;
        case 2: op1 = '*'; break;
        }
        switch(OP2)
        {
        case 0: op2 = '+'; break;
        case 1: op2 = '-'; break;
        case 2: op2 = '*'; break;
        }
        
     String equation = ""+n1+""+op1+""+n2+""+op2+""+n3; 
        eq.setText(""+equation);
       
        
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	submit(op1,op2,n1,n2,n3,vib);
            }
        });
        Toast.makeText(this, "Enter your answer", Toast.LENGTH_SHORT).show();
        
    }
        
    public void submit(char op1, char op2, int n1, int n2, int n3, Vibrator v)
    {
    	try {EditText ans = (EditText) findViewById(R.id.answer);
    	final Integer a = Integer.parseInt(ans.getText().toString());
        if(op1=='+'  && op2=='+')
        {
        	int add = n1+n2+n3;
        	if (a==add)
        	{
        		Toast.makeText(this, "Early must you rise. Leave now I must!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		Intent intent = new Intent(Intent.ACTION_MAIN);
        		intent.addCategory(Intent.CATEGORY_HOME);
        		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);v.cancel();
        		startActivity(intent);
        		System.exit(0);
        	}
        	else
        	{
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Happens to every person, sometimes this does.").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }
        else if (op1=='-' && op2=='-')
        {
        	int sub = n1-n2-n3;
        	if (a==sub)
        	{
        		Toast.makeText(this, "Early must you rise. Leave now I must!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		Intent intent = new Intent(Intent.ACTION_MAIN);
        		intent.addCategory(Intent.CATEGORY_HOME);
        		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);v.cancel();
        		startActivity(intent);
        		System.exit(0);
        	}
        	else
        	{
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Happens to every person, sometimes this does.").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }
        else if(op1=='*' && op2=='*')
        {
        	int mul = n1*n2*n3;
        	if (a==mul)
        	{
        		Toast.makeText(this, "Early must you rise. Leave now I must!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		Intent intent = new Intent(Intent.ACTION_MAIN);
        		intent.addCategory(Intent.CATEGORY_HOME);
        		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);v.cancel();
        		startActivity(intent);
        		System.exit(0);
        	}
        	else
        	{
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Happens to every person, sometimes this does.").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }
        else if(op1=='+' && op2=='-')
        {
        	int mul = n1+n2-n3;
        	if (a==mul)
        	{
        		Toast.makeText(this, "Early must you rise. Leave now I must!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		Intent intent = new Intent(Intent.ACTION_MAIN);
        		intent.addCategory(Intent.CATEGORY_HOME);
        		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);v.cancel();
        		startActivity(intent);
        		System.exit(0);
        	}
        	else
        	{
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Happens to every person, sometimes this does.").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }
        else if(op1=='+' && op2=='*')
        {
        	int mul = n1+n2*n3;
        	if (a==mul)
        	{
        		Toast.makeText(this, "Early must you rise. Leave now I must!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		Intent intent = new Intent(Intent.ACTION_MAIN);
        		intent.addCategory(Intent.CATEGORY_HOME);
        		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);v.cancel();
        		startActivity(intent);
        		System.exit(0);
        	}
        	else
        	{
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Happens to every person, sometimes this does.").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }
        else if(op1=='-' && op2=='+')
        {
        	int mul = n1-n2+n3;
        	if (a==mul)
        	{
        		Toast.makeText(this, "Early must you rise. Leave now I must!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		Intent intent = new Intent(Intent.ACTION_MAIN);
        		intent.addCategory(Intent.CATEGORY_HOME);
        		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);v.cancel();
        		startActivity(intent);
        		System.exit(0);
        	}
        	else
        	{
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Happens to every person, sometimes this does.").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }
        else if(op1=='-' && op2=='*')
        {
        	int mul = n1-n2*n3;
        	if (a==mul)
        	{
        		Toast.makeText(this, "Early must you rise. Leave now I must!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		Intent intent = new Intent(Intent.ACTION_MAIN);
        		intent.addCategory(Intent.CATEGORY_HOME);
        		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);v.cancel();
        		startActivity(intent);
        		System.exit(0);
        	}
        	else
        	{
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Happens to every person, sometimes this does.").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }
        else if(op1=='*' && op2=='-')
        {
        	int mul = n1*n2-n3;
        	if (a==mul)
        	{
        		Toast.makeText(this, "Early must you rise. Leave now I must!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		Intent intent = new Intent(Intent.ACTION_MAIN);
        		intent.addCategory(Intent.CATEGORY_HOME);
        		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);v.cancel();
        		startActivity(intent);
        		System.exit(0);
        	}
        	else
        	{
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Happens to every person, sometimes this does.").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }
        else if(op1=='*' && op2=='+')
        {
        	int mul = n1*n2+n3;
        	if (a==mul)
        	{
        		Toast.makeText(this, "Early must you rise. Leave now I must!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		Intent intent = new Intent(Intent.ACTION_MAIN);
        		intent.addCategory(Intent.CATEGORY_HOME);
        		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        		v.cancel();
        		startActivity(intent);
        		System.exit(0);
        		 
        	}
        	else
        	{
        		
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Happens to every person, sometimes this does.").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }}
    	catch(Exception E)
    	{
    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
  			alertDialogBuilder.setTitle("No Number???");
  			alertDialogBuilder.setMessage("Why you no enter number?!?!?! Sleepy bum!").setCancelable(false).setPositiveButton("Sorry!! :0",new DialogInterface.OnClickListener() 
  			{
  			public void onClick(DialogInterface dialog,int id) 
  				{
  				Intent intent = getIntent();
  				finish();
  				startActivity(intent);
  				}
  			});
  			AlertDialog alertDialog = alertDialogBuilder.create();
  			alertDialog.show();
    	}
        
    }
    public void onPause(Bundle savedInstanceState) 
    {
    	  final Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
          vib.vibrate(new long[] { 0, 200, 0 }, 0);
    }
  
    

    

}
