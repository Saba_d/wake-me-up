package saba.app.wakemeup;

import java.util.Random;

import saba.app.wakemeup.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Easy extends Activity {
	public char op;

    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.easy);
        TextView n1tv = (TextView) findViewById(R.id.n1);
        TextView optv = (TextView) findViewById(R.id.op);
        TextView n2tv = (TextView) findViewById(R.id.n2);
        
        Button submit = (Button) findViewById(R.id.submit);
        EditText et=(EditText) findViewById(R.id.answer);
        Typeface tf1 =   Typeface.createFromAsset(getAssets(),"ALGER.TTF");
        Typeface tf = Typeface.createFromAsset(getAssets(),"ALGER.TTF");
        et.setTypeface(tf);
        optv.setTypeface(tf1); 
        n2tv.setTypeface(tf);
        n1tv.setTypeface(tf);
        
        final int n1 = new Random().nextInt(20);
        final int n2 = new Random().nextInt(20);
        final int OP = new Random().nextInt(3);
        final Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vib.vibrate(new long[] { 0, 200, 0 }, 0);
        
        switch(OP)
        {
        case 0: op = '+'; break;
        case 1: op = '-'; break;
        case 2: op = '*'; break;
        }
        
        n1tv.setText(""+n1);
        n2tv.setText(""+n2);
       
        optv.setText(""+op);
        
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	submit(op,n1,n2,vib);
            }
        });
        
    }
        
    public void submit(char op, int n1, int n2, Vibrator v)
    {
    	try {
    		EditText ans = (EditText) findViewById(R.id.answer);
    	final Integer a = Integer.parseInt(ans.getText().toString());
        if(op=='+')
        {
        	int add = n1+n2;
        	if (a==add)
        	{
        		Toast.makeText(this, "Good! Already you know that, which you need!!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		
        		Intent i = new Intent(this, Med.class);
        		startActivity(i);
        		System.exit(0);
        	}
        	else
        	{
        		//Toast.makeText(this, "Wrong! Do or do not. there is no try!", Toast.LENGTH_SHORT).show();
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Do or do not. there is no try!").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }
        else if (op=='-')
        {
        	int sub = n1-n2;
        	if (a==sub)
        	{
        		Toast.makeText(this, "Good! Already you know that, which you need!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		
        		Intent i = new Intent(this, Med.class);
        	    startActivity(i);
        	    System.exit(0);
        	}
        	else
        	{
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Do or do not. there is no try!").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }
        else if(op=='*')
        {
        	int mul = n1*n2;
        	if (a==mul)
        	{
        		Toast.makeText(this, "Good! Already you know that, which you need!!", Toast.LENGTH_SHORT).show();
        		v.cancel();
        		
        		Intent i = new Intent(this, Med.class);
        		startActivity(i);
        		System.exit(0);
        	}
        	else
        	{
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      			alertDialogBuilder.setTitle("Wrong Answer");
      			alertDialogBuilder.setMessage("Wrong! Do or do not. there is no try!").setCancelable(false).setPositiveButton("Try again!",new DialogInterface.OnClickListener() 
      			{
      			public void onClick(DialogInterface dialog,int id) 
      				{
      				Intent intent = getIntent();
      				finish();
      				startActivity(intent);
      				}
      			});
      			AlertDialog alertDialog = alertDialogBuilder.create();
      			alertDialog.show();
        	}
        }}
    	catch(Exception E)
    	{
    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
  			alertDialogBuilder.setTitle("No Number???");
  			alertDialogBuilder.setMessage("Why you no enter number?!?!?! Sleepy bum!").setCancelable(false).setPositiveButton("Sorry!! :0",new DialogInterface.OnClickListener() 
  			{
  			public void onClick(DialogInterface dialog,int id) 
  				{
  				Intent intent = getIntent();
  				finish();
  				startActivity(intent);
  				}
  			});
  			AlertDialog alertDialog = alertDialogBuilder.create();
  			alertDialog.show();
    	}
    }
    public void onPause(Bundle savedInstanceState) 
    {
    	  final Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
          vib.vibrate(new long[] { 0, 200, 0 }, 0);
    }
    
    


}
